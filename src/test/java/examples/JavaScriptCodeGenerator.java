package examples;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class JavaScriptCodeGenerator extends Generator<String> {
    public JavaScriptCodeGenerator() {
        super(String.class);
    }

    private GenerationStatus status;
    private static final int MAX_IDENTIFIERS = 100;
    private static final int MAX_EXPRESSION_DEPTH = 10;
    private static final int MAX_STATEMENT_DEPTH = 6;
    private static Set<String> identifiers;
    private int statementDepth;
    private int expressionDepth;

    private static final String[] UNARY_TOKENS = {
            "!","++","--","~","delete","new","typeof"
    };

    private static final String[] BINARY_TOKENS = {
            "!=", "!==", "%", "%=", "&", "&&", "&=", "*", "*=", "+", "+=", ",",
            "-", "-=", "/", "/=", "<", "<<", ">>=", "<=", "=", "==", "===",
            ">", ">=", ">>", ">>=", ">>>", ">>>=", "^", "^=", "|", "|=", "||",
            "in", "instanceof"
    };

    @Override
    public String generate(SourceOfRandomness random, GenerationStatus status) {
        this.status = status;
        this.identifiers = new HashSet<>();
        this.statementDepth = 0;
        this.expressionDepth = 0;
        return generateStatement(random).toString();
    }

    static List<String> generateItems(Function<SourceOfRandomness,String> genMethod, SourceOfRandomness random, int mean){
        int len = random.nextInt(mean*2);
        List<String> items = new ArrayList<>(len);

        for(int i = 0; i < len; i++){
            items.add(genMethod.apply(random));
        }

        return items;
    }

    private String generateStatement(SourceOfRandomness random) {
        statementDepth++;
        String result;
        if(statementDepth >= MAX_STATEMENT_DEPTH || random.nextBoolean()) {
            result = random.choose(Arrays.<Function<SourceOfRandomness,String>> asList(
                    this::generateExpressionStatement,
                    this::generateBreakNode,
                    this::generateContinueNode,
                    this::generateReturnNode,
                    this::generateThrowNode,
                    this::generateVarNode,
                    this::generateEmptyNode
            )).apply(random);
        } else {
            result = random.choose(Arrays.<Function<SourceOfRandomness,String>> asList(
                    this::generateIfNode,
                    this::generateForNode,
                    this::generateWhileNode,
                    this::generateNamedFunctionNode,
                    this::generateSwitchNode,
                    this::generateTryNode,
                    this::generateBlock
            )).apply(random);
        }

        statementDepth--;
        return result;
    }

    private String generateExpression(SourceOfRandomness random){
        expressionDepth++;
        String result;
        if(expressionDepth >= MAX_EXPRESSION_DEPTH || random.nextBoolean()){
            result  = random.choose(Arrays.<Function<SourceOfRandomness, String>>asList(
                    this::generateLiteralNode,
                    this::generateIdentNode
            )).apply(random);
        } else {
            result  = random.choose(Arrays.<Function<SourceOfRandomness, String>>asList(
                    this::generateBinaryNode,
                    this::generateUnaryNode,
                    this::generateTernaryNode,
                    this::generateCallNode,
                    this::generateFunctionNode,
                    this::generatePropertyNode,
                    this::generateIndexNode,
                    this::generateArrowFunctionNode
            )).apply(random);
        }

        expressionDepth--;
        return "(" + result + ")";
    }

    private String generateUnaryNode(SourceOfRandomness random) {
        return random.choose(UNARY_TOKENS) + " " + generateExpression(random);
    }

    private String generateArrowFunctionNode(SourceOfRandomness random) {
        String params = "(" + String.join(", ",generateItems(this::generateIdentNode,random, 3)) + ")";

        return random.nextBoolean() ? params + "=>" + generateBlock(random) :
                params + "=>" + generateExpression(random);

    }

    private String generateIndexNode(SourceOfRandomness random) {
        return generateExpression(random) + "[" + generateExpression(random) + "]";
    }

    private String generatePropertyNode(SourceOfRandomness random) {
        return generateExpression(random) + "." + generateIdentNode(random);
    }

    private String generateFunctionNode(SourceOfRandomness random) {
        return "function(" +
                String.join(", ",generateItems(this::generateIdentNode,random,5)) + ")";
    }

    private String generateCallNode(SourceOfRandomness random) {
        String func = generateExpression(random);
        String args = String.join(",",generateItems(this::generateExpression,random,3));

        String call = func + "(" + args + ")";

        return random.nextBoolean() ? call : "new " + call;
    }

    private String generateTernaryNode(SourceOfRandomness random) {
        return generateExpression(random) + " ? " + generateExpression(random) + " : " + generateExpression(random);
    }

    private String generateBinaryNode(SourceOfRandomness random) {
        /*String token = random.choose(BINARY_TOKENS);
        String lhs = generateExpression(random);
        String rhs = generateExpression(random);

        return lhs + " " + token + " " + rhs;*/
        return String.join(" ",
                generateExpression(random),
                random.choose(BINARY_TOKENS),
                generateExpression(random));

    }

    private String generateIdentNode(SourceOfRandomness random) {
        String identifier;

        if(identifiers.isEmpty() || (identifiers.size() < MAX_IDENTIFIERS && random.nextBoolean())) {
            identifier = random.nextChar('a','z') + "_" + identifiers.size();
            identifiers.add(identifier);
        } else{
            identifier = random.choose(identifiers);
        }

        return identifier;
    }

    private String generateObjectProperty(SourceOfRandomness random){
        return generateIdentNode(random) + ": " + generateExpression(random);
    }

    private String generateLiteralNode(SourceOfRandomness random) {
        if(expressionDepth < MAX_EXPRESSION_DEPTH && random.nextBoolean()){
            if (random.nextBoolean()) {
                return "[" + String.join(", ",generateItems(this::generateExpression,random,3)) + "]";
            } else {
                return "{" + String.join(", ",generateItems(this::generateObjectProperty,random,3)) + "}";
            }
        } else {
            return random.choose(Arrays.<Supplier<String>>asList(
                    () -> String.valueOf(random.nextInt(-10,1000)),
                    () -> String.valueOf(random.nextBoolean()),
                    () -> generateStringLiteral(random),
                    () -> "undefined",
                    () -> "null",
                    () -> "this"
            )).get();
        }
    }

    private String generateStringLiteral(SourceOfRandomness random) {
        return '"' + gen().type(String.class).generate(random,status) + '"';
    }

    private String generateExpressionStatement(SourceOfRandomness random) {
        return generateExpression(random);
    }

    private String generateBlock(SourceOfRandomness random) {
        return "{" + String.join(";",generateItems(this::generateStatement,random,4)) + "}";
    }

    private String generateCatchNode(SourceOfRandomness random){
        return "catch (" + generateIdentNode(random) + ") " + generateBlock(random);
    }

    private String generateTryNode(SourceOfRandomness random) {
        return "try " + generateBlock(random) + generateCatchNode(random);
    }

    private String generateCaseNode(SourceOfRandomness random){
        return "case " + generateExpression(random) + ": " + generateBlock(random);
    }

    private String generateSwitchNode(SourceOfRandomness random) {
        return "switch("  + generateExpression(random) + ") {" +
                String.join(" ",generateItems(this::generateCaseNode,random,2)) +
                "}";
    }

    private String generateNamedFunctionNode(SourceOfRandomness random) {
        return "function " + generateIdentNode(random) + "(" +
                String.join(", ",generateItems(this::generateIdentNode,random,5)) + ")";
    }

    private String generateWhileNode(SourceOfRandomness random) {
        return "while (" + generateExpression(random) + ")" + generateBlock(random);
    }

    private String generateForNode(SourceOfRandomness random) {
        String s = "for(";
        if(random.nextBoolean()) {
            s += generateExpression(random);
        }

        s += ";";

        if(random.nextBoolean()){
            s += generateExpression(random);
        }

        s+= ";";

        if(random.nextBoolean()) {
            s += generateExpression(random);
        }

        s += ")";

        s += generateBlock(random);

        return s;
    }

    private String generateIfNode(SourceOfRandomness random) {
        return "if" + "(" + generateExpression(random) + ")" + generateBlock(random) +
                (random.nextBoolean() ? "else " + generateBlock(random) : "");
    }

    private String generateEmptyNode(SourceOfRandomness random) {
        return "";
    }

    private String generateVarNode(SourceOfRandomness random) {
        return "var " + generateIdentNode(random);
    }

    private String generateThrowNode(SourceOfRandomness random) {
        return "throw " + generateExpression(random);
    }

    private String generateReturnNode(SourceOfRandomness random) {
        return "return" + (random.nextBoolean() ? "" : generateExpression(random));
    }

    private String generateContinueNode(SourceOfRandomness random) {
        return "continue";
    }

    private String generateBreakNode(SourceOfRandomness random) {
        return "break";
    }
}
